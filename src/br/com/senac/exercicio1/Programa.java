package br.com.senac.exercicio1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Programa {

    /*
    
    1. Desenvolva um programa que leia o nome e a idade de 3 pessoas e mostre o nome da
        pessoa mais velha e o nome da pessoa mais nova.

     */
    public static void main(String[] args) {

        
        
        
        Pessoa pessoa1 = new Pessoa("Messi", 31);
        Pessoa pessoa2 = new Pessoa("CR7", 34);
        Pessoa pessoa3 = new Pessoa("Neymar", 26);

        List<Pessoa> lista = new ArrayList<>();
        lista.add(pessoa1);
        lista.add(pessoa2);
        lista.add(pessoa3);

        Pessoa pessoaMaisNova = getPessoaMaisNova(lista);
        Pessoa pessoaMaisVelha = getPessoaMaisVelha(lista);

        System.out.println("Pessoa Mais nova é " + pessoaMaisNova.getNome());
        System.out.println("Pessoa Mais velha é " + pessoaMaisVelha.getNome());

    }

    public static Pessoa getPessoaMaisNova(List<Pessoa> lista) {

        Pessoa pessoaMaisNova = lista.get(0);

        for (Pessoa p : lista) {
            if (p.getIdade() < pessoaMaisNova.getIdade()) {
                pessoaMaisNova = p;
            }
        }

        return pessoaMaisNova;
    }

    public static Pessoa getPessoaMaisVelha(List<Pessoa> lista) {

        Pessoa pessoaMaisVelha = lista.get(0);

        for (Pessoa p : lista) {
            if (p.getIdade() > pessoaMaisVelha.getIdade()) {
                pessoaMaisVelha = p;
            }
        }

        return pessoaMaisVelha;
    }

}
