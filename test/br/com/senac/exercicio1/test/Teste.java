package br.com.senac.exercicio1.test;

import br.com.senac.exercicio1.Pessoa;
import br.com.senac.exercicio1.Programa;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class Teste {

    public Teste() {

    }

    @Test
    public void neymarDeveSerMaisNovo() {

        List<Pessoa> lista = new ArrayList<>();
        Pessoa messi = new Pessoa("Messi", 31);
        Pessoa cr7 = new Pessoa("CR7", 34);
        Pessoa neymar = new Pessoa("Neymar", 26);

        lista.add(messi);
        lista.add(cr7);
        lista.add(neymar);

        Pessoa pessoaMaisNova = Programa.getPessoaMaisNova(lista);

        assertEquals(neymar, pessoaMaisNova);

    }

    @Test
    public void messiNaoDeveSerMaisNovo() {

        List<Pessoa> lista = new ArrayList<>();
        Pessoa messi = new Pessoa("Messi", 31);
        Pessoa cr7 = new Pessoa("CR7", 34);
        Pessoa neymar = new Pessoa("Neymar", 26);

        lista.add(messi);
        lista.add(cr7);
        lista.add(neymar);

        Pessoa pessoaMaisNova = Programa.getPessoaMaisNova(lista);

        assertNotEquals(messi, pessoaMaisNova);

    }
    
    
    @Test
    public void cr7DeveSerMaisVelho() {
        List<Pessoa> lista = new ArrayList<>();
        Pessoa messi = new Pessoa("Messi", 31);
        Pessoa cr7 = new Pessoa("CR7", 34);
        Pessoa neymar = new Pessoa("Neymar", 26);

        lista.add(messi);
        lista.add(cr7);
        lista.add(neymar);

        Pessoa pessoaMaisVelha = Programa.getPessoaMaisVelha(lista);
        assertEquals(cr7, pessoaMaisVelha);

    }

    @Test
    public void neymarNaoDeveSerMaisNovo() {
        List<Pessoa> lista = new ArrayList<>();
        Pessoa messi = new Pessoa("Messi", 31);
        Pessoa cr7 = new Pessoa("CR7", 34);
        Pessoa neymar = new Pessoa("Neymar", 26);

        lista.add(messi);
        lista.add(cr7);
        lista.add(neymar);

        Pessoa pessoaMaisVelha = Programa.getPessoaMaisVelha(lista);
        assertNotEquals(neymar, pessoaMaisVelha);

    }

}
